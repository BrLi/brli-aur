# brli-aur

[![License: CC BY-SA 4.0](https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg)](http://creativecommons.org/licenses/by-sa/4.0/)

Copyright (c) 2017-2020 Brli

This is a customized packaging source script repository for Arch Linux (r), especially the packages managed by me under [AUR](https://aur.archlinux.org).

I'm not in any way affiliated with Arch Linux, and I'm not responsible for any damage due to your usage of my scripts in this git repo.

If not specified on the plain text files under this repo, they should be considered as CC BY-SA 4.0.

Upstreams(maintainers on AUR, CCR) may license their works differently, please consider issue a ticket, and I'll add them accordingly in the branch, thank you.
